def hash(input):

    return '{0:04b}'.format( ( int(input[0:4],2)**2 + int(input[4:8],2)**2 + 7 ) % 15 )

def md(msg):

    padding = str(bin(len(msg)))[2:]

    print("padding: " + padding)

    msg = msg + padding

    print("msg: " + msg)

    i = 0

    X = ""

    H = "0000"

    while i < len(msg):
        
            
          X = "00" + H + msg[i:i+2]

          H = hash(X)

          print("i: " + str(i) + " hash: " + H)

          i += 2

    return H


print("final 4-bit hash: " + md("10101001010110"))

print("\n\n")

msg = "10101001010110"

print("x: " + msg)

k_plus_xor_ipad = "0011"

h_k_plus_xor_ipad_concatenate_msg = md(k_plus_xor_ipad + msg)

print("inner hash: " + h_k_plus_xor_ipad_concatenate_msg)

k_plus_opad = "1100"

hmac_msg = k_plus_opad + h_k_plus_xor_ipad_concatenate_msg

print("hmac_msg: " + hmac_msg)

hmac_output = md(hmac_msg)

print("hmac_output: " + hmac_output)
