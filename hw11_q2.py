def hash(input):

    return '{0:04b}'.format( ( int(input[0:4],2)**2 + int(input[4:8],2)**2 + 7 ) % 15 )

def md(msg):

    padding = str(bin(len(msg)))[2:]

    print("padding: " + padding)

    msg = msg + padding

    print("msg: " + msg)

    i = 0

    X = ""

    H = "0000"

    while i < len(msg):
        
            
          X = "00" + H + msg[i:i+2]

          H = hash(X)

          print("i: " + str(i) + " hash: " + H)

          i += 2

    return H


print("final 4-bit hash: " + md("10101001010110"))

print('\n')

# The following is the hash value when the first bit is flipped to 0:

print("final 4-bit hash after first bit is flipped to 0: " + md("00101001010110"))

